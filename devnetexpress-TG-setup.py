##This is heavly modified script from its original form developed by TG team and Ben G.
# This script will help activate, deactive and reset API keys for already existing student accounts
# For DevNet Express Security Track Labs.


import logging, requests, sys, configparser, argparse, json


    
def getopts(argv):
    parser = argparse.ArgumentParser(
        description='ANTI-TEDIUM: Automated iNtuitive Threatgrid Interactions - Total Educational Deployment In Under a Minute',
        epilog='''This script can be used by Organization Administrators to automate the creation and management of student accounts in a dedicated classroom TG Organization. USE WITH CARE -
                It will take the requested action in whatever organization is associated with the provided Admin API key.''' 
        )
    #program level options
    config=parser.add_argument_group('Configuration','Program and API configuration settings')
    config.add_argument('-c', '--cfg_file', help='specify a configuration file (default %(default)s)',
                        type=argparse.FileType('r'), default='anti-tedium.cfg')
    config.add_argument('-l', '--log_file', help='specify a log file (overrides config file)')
    config.add_argument('-k', '--api_key', help='specify an API key value (overrides config file)')
    config.add_argument('-s', '--server_name', help='specify a server hostname (overrides config file)')
    config.add_argument('-v', '--verbose', help='print diagnostic and troubleshooting information to stdout. Once for a reasonable amount, more for lots (0-3)',
                        action='count', default=0)
    config.add_argument('-student', '--student', help='student you want to activate or deactivate')
    
    #account action options
    acct_actions=parser.add_argument_group('Account Actions','Actions to take on non-admin accounts')
    acct_actions.add_argument('-acuser', '--activate-user', help='Activate a deactived user in org',action='store_true')
    acct_actions.add_argument('-dcuser', '--deactivate-user', help='Activate a deactived user in org',action='store_true')
    config.add_argument('-r', '--reset_apikey', help= 'Use this to reset api key for the student this argument requires -student-api-key as well', action='store_true')
    #optional actions that don't change any org settings
    misc_options=parser.add_argument_group('Misc Actions','''Actions that don't affect user accounts but do have an output''')
    misc_options.add_argument('--print_roster', help='print out a list of all non-admin accounts and their corresponding API keys',
                        action='store_true')
    #make with the args already
    args = parser.parse_args()
    #print args
    return(args)


def verbose(msg, vlvl=1):
    if args['verbose'] >=vlvl: print(msg,'\n')

def myget(url,QS):
    try:
        verbose('myget function trying received data: url="'+url+'"; QS="'+QS+'"',2)
        r = requests.get(url,params=QS)
        verbose('Request URL:'+ r.url+'\n',2)
        if r.status_code // 100 != 2:
            return "Error: {}".format(r)
        try:
            return r.json()
        except:
            return "Error: Non JSON response - {}".format(r.text)
    except requests.exceptions.RequestException as e:
        return 'Error: Exception - {}'.format(e)

def myput(url,data):
    try:
        verbose('myput function trying received data: url="'+url+'"; data="'+data+'"',2)
        r = requests.put(url,data=data)
        verbose('Request URL:'+ r.url+'\n',2)
        if r.status_code // 100 != 2:
            return "Error: {}".format(r.text)
        return(str(r.status_code)+'\n'+r.text)
    except requests.exceptions.RequestException as e:
        return 'Error: Exception - {}'.format(e)

def mydelete(url,data):
    try:
        verbose('mydelete function trying received data: url="'+url+'"; data="'+data+'"',2)
        r = requests.delete(url,data=data)
        verbose('Request URL:'+ r.url+'\n',2)
        if r.status_code // 100 != 2:
            return "Error: {}".format(r)
        return(str(r.status_code)+'\n'+r.text)
    except requests.exceptions.RequestException as e:
        return 'Error: Exception - {}'.format(e)
    
    
def mypost(url, data):
    verbose('mypost function trying received data: url="'+url+'"; data="'+data+'"',2)
    try:
        r = requests.post(url, data=data)
        verbose('Request URL:'+ r.url+'\n',2)
        if r.status_code // 100 != 2:
            return "Error: {}".format(r)
        return(str(r.status_code)+'\n'+r.text)

    except requests.exceptions.RequestException as e:
        return 'Error: Exception - {}'.format(e)

def getOrgID():
    verbose('getting your organization ID')
    data=(myget(finalConfig['root_path']+finalConfig['whoami_path'],'api_key='+finalConfig['api_key']))
    verbose(data)
    return(data['data']['organization_id'])

#get student list (all non-admin accts in org)
def getRoster():
    verbose('getting student roster')
    roster=list()
    url=finalConfig['root_path']+finalConfig['org_path']+'/'+orgID+finalConfig['roster_ep']
    reply=myget(url,'api_key='+finalConfig['api_key'])
    allUsers=reply['data']['users']
    for user in allUsers:
        if user['role'] == 'user':
            verbose('user login: '+user['login']+' role: '+user['role']+'... user is a student',3)
            roster.append(user)
        else: verbose('user login: '+user['login']+' role: '+user['role']+'... user is not a student',3)
    return(sorted(roster, key=lambda k: k['login']))
        
##get ready
# get cmdline options
args=vars(getopts(sys.argv))
verbose('\n'.join(['Command line options, including defaults','\n'.join('{}={}'.format(key, val) for key, val in args.items())]),2)



if args['activate_user'] is True and args['deactivate_user'] is True:
    print('Not going to activate and also deactivate  non-admin user. Make up your mind or call with -h for usage help')
    exit()

if not (args['activate_user'] or args['deactivate_user']  or args['reset_apikey'] or args['print_roster']):
    print('Nothing to do! Call with -h for usage help. Exiting.')
    exit()

# Read config file to get settings
verbose('reading config file '+args['cfg_file'].name)
config = configparser.RawConfigParser()
config.read(args['cfg_file'].name)

#get all the main config
mainConfig=dict(config.items('PROGRAM'))

#get all the class config
classConfig=dict(config.items('CLASS'))

#get all the server config
serverConfig=dict(config.items('SERVER'))

#glom together
finalConfig=mainConfig.copy()
finalConfig.update(classConfig)
finalConfig.update(serverConfig)

#overwrite config file with cmdline args where applicable
for key,val in args.items():
    if val is not None:
        finalConfig[key]=val

verbose('\n'.join(['Final config, including defaults','\n'.join('{}={}'.format(key, val) for key, val in finalConfig.items())]),2)
#print finalConfig

##do some preliminary construction
#make reusable root path
finalConfig['root_path']='https://'+finalConfig['server_name']+finalConfig['api_path']
#get orgID
orgID=str(getOrgID())
verbose('Retreived orgID of '+orgID)

##do the thing(s)


#Selectively activate and deactivate users and reset the api key
if finalConfig['activate_user'] is True or finalConfig['deactivate_user'] is True or finalConfig['reset_apikey'] is True:
    data='{"api_key": "'+finalConfig['api_key']+'"}'
    url=finalConfig['root_path']+finalConfig['user_path']+'/'+finalConfig['student']+'/active'
    if finalConfig['activate_user'] is True:
        action=['activate', 'PUT']
        r=myput(url, data)
        finalConfig['print_roster']=True
    elif finalConfig['reset_apikey'] is True:
        url=finalConfig['root_path']+finalConfig['user_path']+'/'+finalConfig['student']+'/api-key'
        r=myput(url, data)
        finalConfig['print_roster']=True
        verbose('server response is: '+r,2)
    else:
        action=['de-activate', 'DELETE']
        r=mydelete(url, data)
        finalConfig['print_roster']=True
    #print('preparing to '+action[0]+'  student account')
    


       

#if spec'd, print class roster
if finalConfig['print_roster'] is True:
    #make header row pretty :/
    pad=' '*(len(finalConfig['username_prefix'])+len(finalConfig['num_students'])-len('STUDENT_LOGIN'))
    print('\n'+pad+'STUDENT_LOGIN\t\tAPI_KEY\t\tACTIVE?'+'\n')
    #print row per student, double spaced
    for student in getRoster():
        print (student['login']+'\t\t'+student['api_key']+'\t\t'+str(student['active'])+'\n')
        
exit()


