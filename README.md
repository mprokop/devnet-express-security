# DevNet Express for Security v1.1 - Internal repository

This is the internal repository for DevNet Express for Security event which we run in Natilik London office 25th and 26th February 2020. Please us this for uploading any code you will be working with during your sessions.

#### Event Team

&nbsp;
<img src="img/team.png"
     alt="Team"
     style="float: left; margin-right: 10px;" />

&nbsp;
#### Relevant links
###### Devnet Express page
https://developer.cisco.com/learning/devnet-express/devnet-express-security-v1-1 <br/>

###### dCloud page
https://dcloud2-lon.cisco.com/content/demo/373252?returnPathTitleKey=content-view&isLoggingIn=true <br/>

###### Natilik Sharepoint folder
https://natilik.sharepoint.com/teams/DevNet/Shared%20Documents/Forms/AllItems.aspx?viewid=015b2118%2Dc984%2D43cf%2D85b2%2Daa1dcaace56e&id=%2Fteams%2FDevNet%2FShared%20Documents%2FDevNet%20Express%20Security <br/>

#### To-do
- [ ] Build the agenda (HK)
- [ ] Confirm with Cisco if we can swap AMP session with FP session (HK)
- [x] Who will do what session (MP)
- [ ] Rebranding slides (HK)
- [ ] Prepare Umbrella license for DNE (MG)

#### WebEx Teams test room details
Please provide these details to your class for any scripts which requires sending message to WebEx Teams:
```
webex_teams_room: DNE Security - Challenge room
webex_teams_room_id: Y2lzY29zcGFyazovL3VzL1JPT00vYTAwNzAzOTAtNDkzNS0xMWVhLTkxNWMtYTk5YTc5NGJiYjE4
```

#### Umbrella Details

##### Umbrella Investigate

Access Token: ```3701a3e5-7e0c-45fe-bc9c-37b7b404c7e8```

##### Umbrella Integration
- ```https://s-platform.api.opendns.com/1.0/events?customerKey=ddd56b8e-d286-4dbc-bcad-47c0523dc12a```
- customerKey: ```ddd56b8e-d286-4dbc-bcad-47c0523dc12a```

##### Read-Only Umbrella Account
Username: ```co-create-uki@cisco.com``` <br/>
Password: ```C!sco12345```

#### ThreatGrid Pre-Reqs

ThreatGrid API Key: ```ogas5m5pea9vlh1nt0adsr2at6```

#### Timetable

###### Tuesday 25th February 2020

 | Time           |Action                          |Who       |
 |----------------|--------------------------------|----------|
 | 07:00 - 08:00  | Registration set up            | HK       |
 |                | Signage set up                 |          |
 | 08:00 - 08:30  | Breakfast delivery and set up  | HK       |
 |                | Food Delivery                  |          |
 |                | Fruit                          |          |
 |                | Coffee                         |          |
 |                | Tea                            |          |
 |                | Milk                           |          |
 |                | Cups / Napkins                 |          |
 |                | Generate WebEx tokens          | MP       |
 | 08:30 - 08:45  | Cisco Arrive                   | CDNT     |
 |                | Presentation set up            | DP AG MP |
 | 08:45 - 09:00  | Guest arrive and registration  | HK       |
 | 09:00 - 09:05  | Housekeeping                   | HK       |
 | 09:05 - 09:30  | Welcome                        | MP       |
 | 09:30 - 10:15  | Getting set up                 | AG       |
 | 10:15 - 10:30  | Coffee Break                   | HK       |
 | 10:30 - 12:45  | Programming Fundamentals       | DP       |
 | 12:45 - 13:30  | Lunch                          | HK       |
 | 13:30 - 14:30  | Intro to Rest APIs             | AB       |
 | 14:30 - 14:45  | Coffee Break                   | HK       |
 | 14:45 - 15:15  | Automating your Zero-Day Threat Hunting workflow: what's it all about?  | MG RP      |
 | 15:15 - 16:15  | An introduction to Cisco Advanced Malware Protection (AMP) for endpoints| RP      |
 | 16:15 - 16:45  | Your Cisco AMP Mission: Use AMP to find rogue endpoints  | RP      |
 | 20:00 - 20:30  | Clear down                     | HK       |

 ###### Wednesday 26th February 2020

| Time           |Action                          |Who       |
|----------------|--------------------------------|----------|
| 08:00 - 08:30 | Breakfast delivery and set up  | HK       |
|               | Food Delivery                  |          |
|               | Fruit                          |          |
|               | Coffee                         |          |
|               | Tea                            |          |
|               | Milk                           |          |
|               | Cups / Napkins                 |          |
|               | Generate WebEx tokens          | MP       |
| 08:30 - 08:45 | Cisco Arrive                   | CDNT     |
|               | Presentation set up            | DP AG MP |
| 08:45 - 09:00 | Guest arrive and registration  | HK       |
| 09:00 - 09:15 | Welcome                        | MP       |
| 09:15 - 10:15 | An introduction to Cisco Identity Services (ISE) APIs | MG       |
| 10:15 - 10:45 | Your Cisco ISE Mission: Nuke the endpoints to Quarantine with ISE | MG       |
| 10:45 - 11:00 | Coffee Break                   | HK       |
| 11:00 - 12:00 | An introduction to Cisco Threat Grid APIs | MP       |
| 12:00 - 12:30 | Your Cisco Threatgrid Mission: Collect Intelligence on malicious SHA using Threat Grid | MP       |
| 12:30 - 13:15 | Lunch                          | HK       |
| 13:15 - 14:15 | An introduction to Cisco Umbrella APIs | MG       |
| 14:15 - 14:45 | Your Cisco Umbrella Mission: Collect Intelligence on the domains using Umbrella Investigate | MG       |
| 14:45 - 15:00 | Coffee Break                   | HK       |
| 15:00 - 16:00 | An introduction to Cisco Firepower APIs   | AG       |
| 16:00 - 16:30 | Your Cisco Firepower Mission: Block malicious domains on the firewall using FDM REST APIs   | AG       |
| 18:30 - 20:30 | Collection of furniture        | AG DP MP |
